public class Rectangle {
    private int width;
    private int heigh;
    private Point topleft;

    public Rectangle(int width, int heigh, Point topleft){
        this.width = width;
        this.heigh = heigh;
        this.topleft = topleft;
    }

    public int area(){
        return this.width * this.heigh;
    }

    public int perimeter(){
        return 2 * (this.heigh + this.width);
    }
    //
    public Point[] corners(){
        Point[] corners = new Point[4];
        corners[0] = this.topleft;
        corners[1] = new Point(topleft.getxCoord() + width, topleft.getyCoord());
        corners[2] = new Point(topleft.getxCoord() , topleft.getyCoord() - heigh);
        corners[3] = new Point(topleft.getxCoord() + width, topleft.getyCoord() - heigh);

        return corners;
    }
}

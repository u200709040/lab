public class TestRectangle {
    public static void main(String[] arguments){
        //

        Rectangle r1 = new Rectangle(10,12,new Point(3,2));

        System.out.println("Area of rectangle is = "+ r1.area());
        System.out.println("Perimeter of rectangle is = "+ r1.perimeter());

        Point[] corners = r1.corners();

        for(int i = 0; i<corners.length;i++){
            Point p = corners[i];

            if(p==null)
                System.out.println("p is null");
            else{
                System.out.println("Corner "+(i+1) + " x = "+p.getxCoord()+ " y = "+p.getyCoord());


            }
        }

    }
}

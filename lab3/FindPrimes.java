public class FindPrimes {
    public static void main(String[] args){

        int max = Integer.parseInt(args[0]);
        //print the numbers less than max
        //for each number less less than max
        for(int number = 2; number<max;number++){
            //Let divisor = 2
            int divisor = 2;
            //Let isPrime = prime
            boolean isPrime = true;
            //while divisor less than number
            while(divisor<number && isPrime) {
                // if the number is divisor number by divisor
                if (number % divisor == 0)
                    //isPrime = false
                    isPrime = false;
                //increment divisor
                divisor++;

            }

            if (isPrime)
                System.out.print(number + " ");





        }


    }
}

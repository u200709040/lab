public class FindGrade {
    public static void main(String[] args){
        //Find Grade
        int score2 = Integer.parseInt(args[0]);

        if(score2>100 || score2<0){
            System.out.println("İnvalid score");
        }
        else if (score2>=90){
            System.out.println("Grade : A");
        }
        else if(score2>=80){
            System.out.println("Grade : B");
        }
        else if(score2>=70){
            System.out.println("Grade : C");
        }
        else if(score2>=60){
            System.out.println("Grade : F");
        }

        //FindGrade2
//         int score = Integer.parseInt(args[0]);
//
//        if(score>= 90 && score<=100){
//            System.out.println("Grade : A");
//        }
//        else if(score>= 80 && score<90){
//            System.out.println("Grade : B");
//        }
//        else if(score>= 70 && score<80){
//            System.out.println("Grade : C");
//        }
//        else if(score>= 60 && score<70){
//            System.out.println("Grade : D");
//        }
//        else if (score>=0 && score<60){
//            System.out.println("Grade : F");
//        }
//        else{
//            System.out.println("It is not a valid score");
//        }


    }
}
